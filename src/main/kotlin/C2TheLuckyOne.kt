import java.io.FileNotFoundException
import java.io.FileReader
import java.io.PrintWriter
import java.util.*

class C2TheLuckyOne {
    private fun solve() {
        val matches = input.nextInt()
        input.nextLine()
        val winners = mutableSetOf<Int>()
        val losers = mutableSetOf<Int>()
        for (match in 1..matches) {
            val player1 = input.nextInt()
            val player2 = input.nextInt()
            val p1Wins = (input.nextInt() == 1)
            input.nextLine()
            val (winner, loser) = if (p1Wins) Pair(player1, player2) else Pair(player2, player1)
            if (winner !in losers) {
                winners.add(winner)
            }
            losers.add(loser)
        }
        output.print(winners.single())
    }

    companion object {
        private const val fileName = "C2TheLuckyOne-submit"
        private const val inputFileName = "$fileName.in"
        private const val outputFileName = "$fileName.out"
        private lateinit var input: Scanner
        private lateinit var output: PrintWriter

        @Throws(FileNotFoundException::class)
        @JvmStatic
        fun main(args: Array<String>) {
            Locale.setDefault(Locale.ENGLISH)
            input = Scanner(FileReader(inputFileName))
            output = PrintWriter(outputFileName)
            val tests = input.nextInt()
            input.nextLine()
            for (t in 1..tests) {
                output.print("Case #$t: ")
                C2TheLuckyOne().solve()
                output.println()
            }
            input.close()
            output.close()
        }
    }
}
