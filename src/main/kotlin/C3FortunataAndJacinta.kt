import java.io.File
import java.io.FileNotFoundException
import java.io.FileReader
import java.io.PrintWriter
import java.math.BigInteger
import java.util.*

class C3FortunataAndJacinta {
    private fun solve() {
        if (input.hasNextInt()) {
            val ranking = input.nextInt()
            input.nextLine()
            val (word, frequency) = wordsByFrequency.entries.elementAt(ranking - 1)
            output.print("$word $frequency")
        } else {
            val word = input.nextLine()
            val frequency = wordsAndFrequency[word]
            val ranking = wordsByFrequency.entries.mapIndexed { index, entry -> if (entry.key == word) index else null }.filterNotNull().single()
            output.print("$frequency #${ranking + 1}")
        }
    }

    companion object {
        private const val fileName = "C3FortunataAndJacinta-submit"
        private const val inputFileName = "$fileName.in"
        private const val outputFileName = "$fileName.out"
        private lateinit var input: Scanner
        private lateinit var output: PrintWriter

        @Throws(FileNotFoundException::class)
        @JvmStatic
        fun main(args: Array<String>) {
            loadCleanData()
            Locale.setDefault(Locale.ENGLISH)
            input = Scanner(FileReader(inputFileName))
            output = PrintWriter(outputFileName)
            val tests = input.nextInt()
            input.nextLine()
            for (t in 1..tests) {
                output.print("Case #$t: ")
                C3FortunataAndJacinta().solve()
                output.println()
            }
            input.close()
            output.close()
        }
    }
}

private const val inputFileName = "pg17013.txt"
private const val outputFileName = "C3FortunataAndJacinta.clean"
private lateinit var input: Scanner
private lateinit var output: PrintWriter
private val wordsAndFrequency = TreeMap<String, BigInteger>()
private lateinit var wordsByFrequency: Map<String, BigInteger>

fun main() {
    cleanInputData()
    loadCleanData()
}

private fun loadCleanData() {
    input = Scanner(FileReader(outputFileName))
    while (input.hasNext()) {
        val word = input.next()
        if (word.length > 2) {
            wordsAndFrequency.merge(word, BigInteger.ONE) { current: BigInteger, default: BigInteger -> current + default }
        }
    }
    wordsByFrequency = wordsAndFrequency.entries.sortedByDescending { it.value }.associateBy({ it.key }, { it.value })
    input.nextLine()
    input.close()
}

private fun cleanInputData() {
    output = PrintWriter(outputFileName)
    File(inputFileName).forEachLine { line ->
        line.toLowerCase().forEach { character ->
            val char = when (character) {
                in 'a'..'z', 'ñ', 'á', 'é', 'í', 'ó', 'ú', 'ü' -> character
                else -> ' '
            }
            output.print(char)
        }
        output.println()
    }
    output.close()
}
