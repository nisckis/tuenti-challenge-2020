La imagen es un cerebro con un texto inferior que dice que no es eso.
En Exif/Comment tenemos un texto que dice que el código no es ese.
Sin embargo si comprobamos el fichero, vemos que hay texto detrás del fin de fichero.
El texto es código Brainfuck, así que si lo ejecutamos por ejemplo en https://fatiherikli.github.io/brainfuck-visualizer obtenemos "THE CODE IS: WOLOLO!!"
