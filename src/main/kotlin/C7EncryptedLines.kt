import java.io.FileNotFoundException
import java.io.FileReader
import java.io.PrintWriter
import java.util.*

@Suppress("SpellCheckingInspection")
class C7EncryptedLines {

    /*
        The audio song is "Symphony No. 9, Op. 95, In E minor, 'From the New World': Allegro" from the artist Antonín Dvořák
        Furthermore if we Google the dates we have in the input file we confirm this text is about Antonín Dvořák
        With the date and knowing the name of the artist we can deduce the cypher is a letter substitution
     */
    private fun solve() {
        val text = input.nextLine().toCharArray()
        for (pos in text.indices) {
            val c = text[pos]
            val indexOf = CYPHER_ALPHABET.indexOf(c)
            if (indexOf != -1) {
                text[pos] = NORMAL_ALPHABET[indexOf]
            }
        }
        output.print(text)
    }

    companion object {
        private const val fileName = "C7EncryptedLines-submit"
        private const val inputFileName = "$fileName.in"
        private const val outputFileName = "$fileName.out"
        private lateinit var input: Scanner
        private lateinit var output: PrintWriter
        private val NORMAL_ALPHABET = " '(),-./0123456789;abcdefghijklmnopqrstuvwxyz".toCharArray()
        private val CYPHER_ALPHABET = " -()w#vz0123456789saxje.uidchtnmbrl'poygk,qf;".toCharArray()
//      private val CYPHER_ALPHABET = "+++++-+++++++++++++++++++++++++++++++++++++++".toCharArray()

        @Throws(FileNotFoundException::class)
        @JvmStatic
        fun main(args: Array<String>) {
            Locale.setDefault(Locale.ENGLISH)
            input = Scanner(FileReader(inputFileName))
            output = PrintWriter(outputFileName)
            val tests = input.nextInt()
            input.nextLine()
            for (t in 1..tests) {
                output.print("Case #$t: ")
                C7EncryptedLines().solve()
                output.println()
            }
            input.close()
            output.close()
        }

    }
}
/*
fun main() {
    val letters = mutableSetOf<Char>()
    for (c in File(C7EncryptedLines.inputFileName).readText().replace('\n', ' ').replace('\r', ' ')) {
        letters.add(c)
    }
    println(letters.toList().sorted())
}
*/
