import java.io.FileNotFoundException
import java.io.FileReader
import java.io.PrintWriter
import java.util.*

class C1RockPaperScissors {
    private fun solve() {
        val player1 = input.next()
        val player2 = input.next()
        input.nextLine()
        val winner = when (player1) {
            player2 -> "-"
            "R" -> if (player2 == "P") "P" else "R"
            "P" -> if (player2 == "S") "S" else "P"
            "S" -> if (player2 == "R") "R" else "S"
            else -> throw IllegalStateException("player1 invalid state $player1")
        }
        output.print(winner)
    }

    companion object {
        private const val fileName = "C1RockPaperScissors-submit"
        private const val inputFileName = "$fileName.in"
        private const val outputFileName = "$fileName.out"
        private lateinit var input: Scanner
        private lateinit var output: PrintWriter

        @Throws(FileNotFoundException::class)
        @JvmStatic
        fun main(args: Array<String>) {
            Locale.setDefault(Locale.ENGLISH)
            input = Scanner(FileReader(inputFileName))
            output = PrintWriter(outputFileName)
            val tests = input.nextInt()
            input.nextLine()
            for (t in 1..tests) {
                output.print("Case #$t: ")
                C1RockPaperScissors().solve()
                output.println()
            }
            input.close()
            output.close()
        }
    }
}
