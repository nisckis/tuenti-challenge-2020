import java.io.File
import java.io.FileNotFoundException
import java.io.PrintWriter
import java.math.BigInteger
import java.util.*

class C12HackermanOrigins {

    /*
        https://crypto.stackexchange.com/questions/43583/deduce-modulus-n-from-public-exponent-and-encrypted-data
        https://rosettacode.org/wiki/RSA_code#Kotlin
     */
    companion object {
        private const val fileName = "C12HackermanOrigins"
        private const val outputFileName = "$fileName.out"
        private lateinit var output: PrintWriter

        @Throws(FileNotFoundException::class)
        @JvmStatic
        fun main(args: Array<String>) {
            Locale.setDefault(Locale.ENGLISH)
            output = PrintWriter(outputFileName)

            val typicalExponents = arrayOf(3, 5, 17, 257, 65537)
            val plainBytes1 = File("testdata/plaintexts/test1.txt").readBytes()
            val plainBytes1B = File("testdata/plaintexts/test1.txt").readText().toByteArray()
            println(plainBytes1.contentEquals(plainBytes1B))
            val plainNum1 = BigInteger(plainBytes1)
            println("plainNum1: $plainNum1")
            val cipherBytes1 = File("testdata/ciphered/test1.txt").readBytes()
            val cipherNum1 = BigInteger(cipherBytes1)
            println("cipherNum1: $cipherNum1")
            val plainBytes2 = File("testdata/plaintexts/test2.txt").readBytes()
            val plainNum2 = BigInteger(plainBytes2)
            println("plainNum2: $plainNum2")
            val cipherBytes2 = File("testdata/ciphered/test2.txt").readBytes()
            val cipherNum2 = BigInteger(cipherBytes2)
            println("cipherNum2: $cipherNum2")
            // N can be obtained from gcd(Me1−C1,Me2−C2).
            for (e in typicalExponents) {
                println("e: $e")
                val n = (plainNum1.pow(e) - cipherNum1).gcd(plainNum2.pow(e) - cipherNum2)
                println("n: $n")
                if ((plainNum1 > n) || (plainNum2 > n)) {
                    println("Plaintext is too long")
                    continue
                }
                output.println(n)
                break
            }
            output.close()
        }
    }
}
