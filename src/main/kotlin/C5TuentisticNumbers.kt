import java.io.FileNotFoundException
import java.io.FileReader
import java.io.PrintWriter
import java.math.BigInteger
import java.util.*

class C5TuentisticNumbers {

    private fun solve() {
        val number = input.nextBigInteger()
        input.nextLine()
        if (number < TWENTY) {
            output.print("IMPOSSIBLE")
            return
        }
        val (quotient, remainder) = number.divideAndRemainder(TWENTY)
        if (quotient.multiply(NINE) < remainder) {
            output.print("IMPOSSIBLE")
            return
        }
        output.print(quotient)
    }

    companion object {
        private const val fileName = "C5TuentisticNumbers-submit"
        private const val inputFileName = "$fileName.in"
        private const val outputFileName = "$fileName.out"
        private lateinit var input: Scanner
        private lateinit var output: PrintWriter
        private val TWENTY = BigInteger("20")
        private val NINE = BigInteger("9")

        @Throws(FileNotFoundException::class)
        @JvmStatic
        fun main(args: Array<String>) {
            Locale.setDefault(Locale.ENGLISH)
            input = Scanner(FileReader(inputFileName))
            output = PrintWriter(outputFileName)
            val tests = input.nextInt()
            input.nextLine()
            for (t in 1..tests) {
                output.print("Case #$t: ")
                C5TuentisticNumbers().solve()
                output.println()
            }
            input.close()
            output.close()
        }

    }
}
