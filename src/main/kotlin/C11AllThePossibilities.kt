import java.io.FileNotFoundException
import java.io.FileReader
import java.io.PrintWriter
import java.util.*

class C11AllThePossibilities {

    /*
        ONCE YOU NOTICE THIS IS THE COIN CHANGE ALGORITHM IT IS EASY TO DO.
        THE countCoins FUNCTION IS FROM http://rosettacode.org/wiki/Count_the_coins#Kotlin
     */
    private fun solve() {
        val x = input.nextInt()
        val vetoedNumbers = input.nextLine().split(' ').filter(String::isNotBlank).map { it.toInt() }.toSet().toIntArray()
        val c = IntArray(x - 1) { it + 1 }.filter { it !in vetoedNumbers }.toIntArray()
        output.print(countCoins(c, c.size, x))
    }

    private fun countCoins(c: IntArray, m: Int, n: Int): Long {
        val table = LongArray(n + 1)
        table[0] = 1
        for (i in 0 until m) {
            for (j in c[i]..n) {
                table[j] += table[j - c[i]]
            }
        }
        return table[n]
    }

    companion object {
        private const val fileName = "C11AllThePossibilities-submit"
        private const val inputFileName = "$fileName.in"
        private const val outputFileName = "$fileName.out"
        private lateinit var input: Scanner
        private lateinit var output: PrintWriter

        @Throws(FileNotFoundException::class)
        @JvmStatic
        fun main(args: Array<String>) {
            Locale.setDefault(Locale.ENGLISH)
            input = Scanner(FileReader(inputFileName))
            output = PrintWriter(outputFileName)
            val tests = input.nextInt()
            input.nextLine()
            for (t in 1..tests) {
                output.print("Case #$t: ")
                C11AllThePossibilities().solve()
                output.println()
            }
            input.close()
            output.close()
        }
    }
}
