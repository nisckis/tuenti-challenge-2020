# [Tuenti Challenge 10][tc10]
*****

Hola, esta es una serie de pequeños análisis, comentarios y anécdotas de los desafios que pude resolver correctamente durante la semana que dura la [Tuenti Challenge][tc10].

Este año he abandonado Java, que llevo usando profesionalmente más de 16 años, y he optado por usar [Kotlin][kotlin] que en mi opinión es Java on drugs (disclaimer: solo es una expresión so... please, don't do drugs).

La verdad es que la experiencia de usar [Kotlin][kotlin] en código real ha sido más que gratificante, si bien después de tantos años con Java cuesta el pensar y escribir código real de una manera distinta.

Este año he acabado en el puesto [105 de 667 participantes][stats] que resolvieron almenos un desafío, que en cierto modo no es una posición ni buena ni mala dado el altísimo nivel de los participantes; solo hay que ver que este año para conseguir una de las preciadas camisetas :shirt: del top 30 ha sido necesario resolver 16 de los 20 desafíos.

Todas las soluciones están disponibles en este mismo [repositorio git][git] y desarrolladas en Kotlin 1.3 corriendo sobre Java 13. Por algún motivo que desconozco la última versión de [Gradle][gradle] no soporta OpenJDK 14.

* [Challenge 1 - Rock, Paper, Scissors][e-c1] ([Write-up](#markdown-header-challenge-1-rock-paper-scissors))
* [Challenge 2 - The Lucky One][e-c2] ([Write-up](#markdown-header-challenge-2-the-lucky-one))
* [Challenge 3 - Fortunata and Jacinta][e-c3] ([Write-up](#markdown-header-challenge-3-fortunata-and-jacinta))
* [Challenge 4 - My Favorite Video Game][e-c4] ([Write-up](#markdown-header-challenge-4-my-favorite-video-game))
* [Challenge 5 - Tuentistic Numbers][e-c5] ([Write-up](#markdown-header-challenge-5-tuentistic-numbers))
* [Challenge 6 - Knight Labyrinth][e-c6] ([No resuelto](#markdown-header-challenge-6-knight-labyrinth))
* [Challenge 7 - Encrypted lines][e-c7] ([Write-up](#markdown-header-challenge-7-encrypted-lines))
* [Challenge 8 - Headache][e-c8] ([Write-up](#markdown-header-challenge-8-headache))
* [Challenge 9 - Just Another Day on Battlestar Galactica][e-c9] ([Write-up](#markdown-header-challenge-9-just-another-day-on-battlestar-galactica))
* [Challenge 10 - Villa Feristela escape room][e-c10] ([Write-up](#markdown-header-challenge-10-villa-feristela-escape-room))
* [Challenge 11 - All the Possibilities][e-c11] ([Write-up](#markdown-header-challenge-11-all-the-possibilities))
* [Challenge 12 - Hackerman Origins][e-c12] ([Write-up](#markdown-header-challenge-12-hackerman-origins))
* [Challenge 13 - The Great Toilet Paper Fortress][e-c13] ([No resuelto](#markdown-header-challenge-13-the-great-toilet-paper-fortress))
* [Challenge 14 - Public Safety Bureau][e-c14] ([No resuelto](#markdown-header-challenge-14-public-safety-bureau))
* [Challenge 15 - The internsheep][e-c15] ([No resuelto](#markdown-header-challenge-15-the-internsheep))
* [Challenge 16 - The new office][e-c16] ([No resuelto](#markdown-header-challenge-16-the-new-office))
* [Challenge 17 - Zatōichi][e-c17] (No desbloqueado)
* [Challenge 18 - Matters with Formatters][e-c18] (No desbloqueado)
* [Challenge 19 - Better Farm Systems][e-c19] (No desbloqueado)
* [Challenge 20 - Fortified Security][e-c20] (No desbloqueado)

## Challenge 1 - Rock, Paper, Scissors
Como es tradicional el primer desafío es sencillo para que todo el mundo se habitúe a la forma general de funcionar de los distintos desafíos:

1. Leer de un fichero de entrada.
2. Procesar cada caso de uso.
3. Escribir el fichero de salida.
4. Enviar el fichero de salida.

Este año el primer desafío va de jugar a piedra, papel, tijeras donde nos dicen que saca cada jugador y tenemos que decir quien gana de los dos, por lo que sencillamente tenemos que ver si han empatado y en caso contrario ver quien de los dos ha ganado.

```
#!kotlin
val player1 = input.next()
val player2 = input.next()
input.nextLine()
val winner = when (player1) {
    player2 -> "-"
    "R" -> if (player2 == "P") "P" else "R"
    "P" -> if (player2 == "S") "S" else "P"
    "S" -> if (player2 == "R") "R" else "S"
    else -> throw IllegalStateException("player1 invalid state $player1")
}
output.print(winner)
```

En este código podemos ver varias cosas interesantes de Kotlin:

- Por un lado tanto `when` como `if` son expresiones, por lo que devuelven un valor.
- Por otro lado `when` no obliga a usar valores fijos con los que comparar, sino que va evaluando cada rama de forma secuencial hasta que encuentra una rama que casa.


## Challenge 2 - The Lucky One
En este desafío nos dan una lista de enfrentamientos de jugadores de ping-pong y quien es el ganador de cada enfrentamiento, teniendo en cuenta que no hay empates, que un jugador de mayor nivel de habilidad siempre vence a uno de menor nivel de habilidad y que hay una única solución. Para cada lista de enfrentamientos nos piden quien es el ganador del torneo.

A primera vista puede parecer tan sencillo como seleccionar el jugador con más nivel, y la gracia del desafío está precisamente en que no nos dan dicho nivel, por lo que tenemos que encontrar que jugador ha ganado todos sus enfrentamientos.

Para ello vamos a abusar de los conjuntos, donde crearemos un conjunto con los ganadores y otro con los perdedores. Así mismo solo añadimos un jugador a la lista de ganadores si no está en la de perdedores.
Lo anterior no nos garantiza que debido al orden de enfrentamientos solo tengamos un ganador, eso nos lo garantiza `winners.single()` que fallará en caso que no haya exactamente 1 resultado.

Otra forma de asegurar siempre que solo hay un ganador sin importar el orden de los datos sería añadiendo `winners.remove(loser)` después de `losers.add(loser)`, lo único es que esto supone aumentar el tiempo de ejecución para todos los casos.

```
#!kotlin
val winners = mutableSetOf<Int>()
val losers = mutableSetOf<Int>()
for (match in 1..matches) {
    val player1 = input.nextInt()
    val player2 = input.nextInt()
    val p1Wins = (input.nextInt() == 1)
    input.nextLine()
    val (winner, loser) = if (p1Wins) Pair(player1, player2) else Pair(player2, player1)
    if (winner !in losers) {
        winners.add(winner)
    }
    losers.add(loser)
}
output.print(winners.single())
```

En este código podemos ver dos cosas interesantes de Kotlin:

- Por un lado el uso de `Pair` para devolver dos valores y no solo uno.
- Por otro lado la [desestructuración](https://kotlinlang.org/docs/reference/multi-declarations.html) de objetos, donde en lugar de asignar un objeto a una variable lo que hacemos es asignar las propiedades de un objeto en distintas variables.
- Así mismo ponemos ver el uso de `in` y `!in` para saber si un objeto está o no en una colección.


## Challenge 3 - Fortunata and Jacinta
En este desafío nos dan el texto en bruto de la obra `Fortunata y Jacinta` que previamente tendremos que limpiar siguiendo unas reglas de sustitución de caracteres y descartando las palabras de menos de 3 letras.

Una vez tengamos limpio el texto, nos piden que para unos casos devolvamos el número de repeticiones y su ranking, y para otros casos la palabra que ocupa cierta posición y su número de repeticiones.

Para resolverlo lo primero que tenemos que hacer es obtener un mapa de las palabras y su número de repeticiones, y una vez lo tenemos generamos el mismo mapa ordenado descendentemente por el número de repeticiones.  
Para calcular el primer mapa usaremos la función [merge](https://docs.oracle.com/en/java/javase/13/docs/api/java.base/java/util/Map.html#merge(K,V,java.util.function.BiFunction)) de los Map, que nos permite realizar el conteo de cada palabra de forma muy sencilla. Además dado que no hay ninguna garantía sobre el máximo número de repeticiones vamos a usar BigInteger.  
Para calcular el segundo mapa abusaremos de Kotlin de forma que primero ordenamos las palabras de forma descendente por número de repeticiones y luego siguiendo dicho orden, cada entrada estará creada por la palabra y su número de repeticiones.

```
#!kotlin
private fun loadCleanData() {
    input = Scanner(FileReader(outputFileName))
    while (input.hasNext()) {
        val word = input.next()
        if (word.length > 2) {
            wordsAndFrequency.merge(word, BigInteger.ONE) { current: BigInteger, default: BigInteger -> current + default }
        }
    }
    wordsByFrequency = wordsAndFrequency.entries.sortedByDescending { it.value }.associateBy({ it.key }, { it.value })
    input.nextLine()
    input.close()
}
```

En cuanto a uso de Kotlin, el hecho de poder reordenar las entradas del mapa y asignarles la clave y valor, todo ello en una sola línea y partiendo de un mapa inicial es realmente bueno. En Java podríamos hacer algo similar usando un comparador específico en el contructor del mapa.

Una vez tenemos los dos mapas cargados, para cada caso simplemente devolvemos los valores que nos piden.

```
#!kotlin
if (input.hasNextInt()) {
    val ranking = input.nextInt()
    input.nextLine()
    val (word, frequency) = wordsByFrequency.entries.elementAt(ranking - 1)
    output.print("$word $frequency")
} else {
    val word = input.nextLine()
    val frequency = wordsAndFrequency[word]
    val ranking = wordsByFrequency.entries.mapIndexed { index, entry -> if (entry.key == word) index else null }.filterNotNull().single()
    output.print("$frequency #${ranking + 1}")
}
```

Para el caso de devolver la frecuencia y el ranking podemos bien hacerlo en dos pasos usando ambos mapas o bien podríamos haberlo hecho simplemente con lo siguiente porque en el segundo mapa el valor que hemos guardado con `associateBy` es la frecuencia y no la entrada original.

```
#!kotlin
val (ranking, frequency) = wordsByFrequency.entries.mapIndexed { index, entry -> if (entry.key == word) Pair(index, entry.value) else null }.filterNotNull().single()
```


## Challenge 4 - My Favorite Video Game
Este es el primer desafío de este año que no requiere programación alguna, recordemos que la Tuenti Challenge **no** es solo una competición de programación sino que evaluan distintos conocimientos, intuición y suerte.

En este desafío nos dan el API para solicitar la clave de los juegos así como una presentación en la que podemos ver la organización de los distintos sistemas.

Según vemos en el diagrama los sistemas de *Producción* y *Preproducción* comparten la misma base de datos, y el frontal del API de ambos sistemas está gestionado por un balanceador de carga.

Si hacemos una petición `curl` como en el ejemplo que nos dan pero habilitando el modo verbose vemos tanto lo que se envía como lo que se recibe.

```
#!bash
curl -v http://steam-origin.contest.tuenti.net:9876/games/cat_fight/get_key
*   Trying 52.49.91.111...
* TCP_NODELAY set
* Connected to steam-origin.contest.tuenti.net (52.49.91.111) port 9876 (#0)
> GET /games/cat_fight/get_key HTTP/1.1
> Host: steam-origin.contest.tuenti.net:9876
> User-Agent: curl/7.55.1
> Accept: */*
>
< HTTP/1.1 403 FORBIDDEN
< Server: nginx/1.17.8
< Date: Sun, 10 May 2020 17:17:05 GMT
< Content-Type: application/json
< Content-Length: 30
< Connection: keep-alive
<
{"error":"Not available yet"}
* Connection #0 to host steam-origin.contest.tuenti.net left intact
```

Como podemos ver en la petición está la cabecera `Host` y en la respuesta tenemos `Server: nginx/1.17.8`.  
Si hacemos una búsqueda de fallos de seguridad en nginx v1.17.8 no encontramos nada interesante que pudiera permitirnos saltar entre servidores, así que debe ser algo con la cabecera de la petición.  
Si buscamos la [cabecera `Host`](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Host) leemos:
> The Host request header specifies the host and port number of the server to which the request is being sent.
 
Lo que significa que podría ser algo tan "tonto" como cambiar el valor de la cabecera para que vaya contra el servidor de Preproducción que no es visible desde el exterior. Probamos a ver y...
```
#!bash
curl -v -H "Host: pre.steam-origin.contest.tuenti.net:9876"  http://steam-origin.contest.tuenti.net:9876/games/cat_fight/get_key
*   Trying 52.49.91.111...
* TCP_NODELAY set
* Connected to steam-origin.contest.tuenti.net (52.49.91.111) port 9876 (#0)
> GET /games/cat_fight/get_key HTTP/1.1
> Host: pre.steam-origin.contest.tuenti.net:9876
> User-Agent: curl/7.55.1
> Accept: */*
>
< HTTP/1.1 200 OK
< Server: nginx/1.17.8
< Date: Sun, 10 May 2020 17:56:39 GMT
< Content-Type: application/json
< Content-Length: 44
< Connection: keep-alive
<
{"game":"Cat Fight","key":"7544-5747-5357"}
* Connection #0 to host steam-origin.contest.tuenti.net left intact
```

Nos devuelve la clave que buscábamos.  
Como pongo en el "código" del desafío, muy realista no es que digamos, porque dudo mucho que de verdad tuviesen un balanceador de carga que permita que se pueda inyectar una cabecera de Host, aunque claro, que tuviesemos que usar un exploit real para resolverlo probablemente sería excesivo.


## Challenge 5 - Tuentistic Numbers
Este desafío es bastante gracioso por lo del juego de palabras entre **Tuenti** y **twenty**, definiéndose los números **Tuentísticos** como aquellos que en Inglés contienen la palabra **twenty**.  
Nos dan una serie de números y nos piden que obtengamos el máximo número de elementos Tuentísticos que suman dichos valores.

Dado que no es un desafío de un algoritmo concreto, lo que necesitamos es encontrar la serie que siguen los distintos números:

- Por un lado sabemos que todo número menor de 20 no puede ser Tuentístico.
- Por otro lado nos piden el máximo por lo que nos interesa sumar el mayor número de veces posible el valor 20.

Lo anterior nos deja con un módulo de 20 y un resto, siendo este resto un valor entre 0 y 19.
Este resto lo tenemos que consumir con números Tuentísticos del 21 al 29 para seguir conservando el valor máximo de números Tuentísticos sumados.  
Eso significa que cada número Tuentístico puede absorber hasta 9 puntos del resto.

Por lo tanto, dado el valor N, si 9 veces el divisor de N y 20 es menor que el valor del resto entonces **no** es posible realizar la suma con número Tuentísticos, y en caso contrario sí.

```
#!kotlin
val number = input.nextBigInteger()
input.nextLine()
if (number < TWENTY) {
    output.print("IMPOSSIBLE")
    return
}
val (quotient, remainder) = number.divideAndRemainder(TWENTY)
if (quotient.multiply(NINE) < remainder) {
    output.print("IMPOSSIBLE")
    return
}
output.print(quotient)
```

Otra forma análoga a la anterior es coger hoja y lápiz e ir escribiendo la secuencia.

- De 1 a 19 es imposible.
- 1 va de 20 a 29.
- 2 va de 40 a 58.
- 3 va de 60 a 87.

Con el 3 ya vemos que este llega al siguiente ciclo de números Tuentísticos, por lo que tendríamos que:

- Son imposibles del 1 al 19, del 30 al 39, y el 59.
- El valor máximo que buscamos es N dividido entre 20 descartando los decimales.


## Challenge 6 - Knight Labyrinth
Este desafío mezcla el atravesar un laberinto para llegar a una posición concreta (la princesa) con el hecho que no nos movemos de uno en uno, sino en L como el caballo de ajedrez.

Al principio puede parecer sencillo, pero es precisamente ese movimiento en L que se salta muros donde está la dificultad, en como garantizar el "avanzar" hacia la solución evitando hacer un bucle.

Después de todo un día solo con este desafío tuve que saltármelo porque no fui capaz de evitar que la forma en que recorría el laberinto generase bucles. 


## Challenge 7 - Encrypted lines
Este es otro desafío que me ha hecho gracia porque la pista inicial para resolverlo es una canción. Así que usamos una aplicación de reconocimiento de canciones y obtenemos que la canción es **Symphony No. 9, Op. 95, In E minor, 'From the New World': Allegro** del artista **Antonín Dvořák**.

Por otro lado, si abrimos el texto a descifrar nos encontramos con fechas como `8 o.ly.mx.p 1841` y `1 maf 1904`, donde la primera por longitud debe ser `8 September 1841`.  
Si buscamos dicha fecha en Google encontramos que efectivamente el texto va sobre Antonín Dvořák. Con la fecha y sabiendo el nombre del artista podemos deducir que el cifrado es por sustitución de letras.

Una vez sabemos lo anterior, con la información disponible en la Wikipedia, podemos usar alguna página para que nos de una aproximación inicial a la clave de sustitución a partir del texto cifrado gracias al análisis de frecuencias del alfabeto inglés.

Una vez tenemos dicha aproximación de la clave, vamos generando refinamientos de la clave con el texto que vamos descifrando en cada refinamiento.

Llegados a este punto, con algo de paciencia y haciendo las oportunas verificaciones acabamos obteniendo la clave final que devuelve el texto descifrado correcto.

```
#!kotlin
private val NORMAL_ALPHABET = " '(),-./0123456789;abcdefghijklmnopqrstuvwxyz".toCharArray()
private val CYPHER_ALPHABET = " -()w#vz0123456789saxje.uidchtnmbrl'poygk,qf;".toCharArray()
```

```
#!kotlin
val text = input.nextLine().toCharArray()
for (pos in text.indices) {
    val c = text[pos]
    val indexOf = CYPHER_ALPHABET.indexOf(c)
    if (indexOf != -1) {
        text[pos] = NORMAL_ALPHABET[indexOf]
    }
}
output.print(text)
```

A nivel anecdótico, había una letra de la clave que no había marcado correctamente y estuve un muy buen rato intentando averiguar que estaba haciendo mal hasta que me di cuenta que dicha letra no la había sustituído de forma correcta. Una vez solventado devolvió la solución correcta.


## Challenge 8 - Headache
Llegamos a un nuevo desafío donde no hay que programar, donde la única pista que tenemos es una imagen en blanco, y nos piden que obtengamos el código.

Si abrimos la imagen con un editor y seleccionamos por color, vemos que en realidad la imagen es un cerebro con un texto inferior que dice que no es eso.  
Si miramos en el campo Exif/Comment de la imagen encontramos un texto que dice que el código no es ese. Sin embargo si comprobamos el fichero, vemos que hay texto detrás del fin de fichero.  
El texto es código Brainfuck, así que si lo ejecutamos por ejemplo en https://fatiherikli.github.io/brainfuck-visualizer obtenemos "THE CODE IS: WOLOLO!!"

Así que este desafío es sencillo si sabes donde buscar y si sabes que el texto que está detrás del fin de fichero de la imagen es código Brainfuck.  
En caso que no sepamos que el código es Brainfuck, si lo copiamos y pegamos en Google este nos dice que es código Brainfuck, y de ahí podemos buscar como ejecutarlo para obtener la solución.


## Challenge 9 - Just Another Day on Battlestar Galactica
Este desafío vuelve a ser un tema de textos cifrados, en este caso nos dan el texto original, el texto cifrado y la función que se usó para cifrar el texto original.

Como vemos el cifrado es un XOR entre el mensaje (por posición ascendente) y la clave (por posición descendente). Debido a las propiedades de XOR, si al texto original le aplicamos un XOR con el texto cifrado obtenemos la clave.

En eso punto podemos usar una calculadora XOR como la de http://xor.pw/ e ir obteniendo los distintos valores de la clave de cifrado.

Una vez tenemos la clave "40614178165780923111223" y dado el nuevo mensaje cifrado en hexadecimal "3A3A333A333137393D39313C3C3634333431353A37363D" repetimos el proceso anterior y obtenemos las coordenadas "981;204;499;905;301;169".

Con las coordenas y la clave podemos ir a https://www.jdoodle.com/test-bash-shell-script-online/ y ejecutar el script para comprobar que obtenemos el mensaje hexadecimal cifrado del que habíamos partido.


## Challenge 10 - Villa Feristela escape room
Pufffff, la verdad es que este desafío ha sido más tedioso de lo que me habría gustado, sobretodo por mi entusiasmo inicial al encontrar un desafío basado en los antiguos MUD con los que algunos crecimos cuando internet era en modo texto.

En general está bien, pero han habido varios puntos bastante molestos.  
Por un lado en la habitación 105 del zombie parece que a veces hay algo roto, como comentan por ejemplo en https://twitter.com/alberto__angulo/status/1255885925479059456, lo que hace que por si acaso cierres la ejecución y vuelvas a empezar.  
Por otro lado, la forma en que se le puede decir el número de cerebros al zombie es única, funcionando `🗣 119 | 💬 🧟` pero fallando `💬 🧟 < 🗣 119`, siendo la segunda la que estaba intentando. Además, por lo general en un MUD lo esperable habría sido `🗣 119 🧟` o incluso simplemente `🗣 119`, pero no, solo ha aceptado la primera de todas las formas anteriores con el pipe.  
Además también está el problema que en una de las ejecuciones he podido hacer `🗣 119 > 🧟` por lo que he "roto" el zombie por completo y me ha tocado empezar nuevamente de cero.

Además de todo lo anterior, está el problema de que los emojis no siempre los coge, por ejemplo el `🗣` no lo ha cogido de la emojipedia pero por suerte, no se si es intencionado o no, el sistema de archivos es navegable, por lo que si ejecutamos `🔦 -la /game/actions/` podemos copiar y pegar de ahí el emoji correcto para poder realizar el echo.

Luego hay un pequeño inconveniente y es que el mago te quita los items en cuanto los tienes todos y vuelves a hablar con él, por lo que si cometes el error de copiar el texto con Control+C, se aborta la ejecución del mago y tendrás que volver a empezar otra vez desde el principio.

Lo bueno es que una vez resuelves todo el misterio y obtienes la clave, puedes probar que simplemente con coger la mariquita del agujero y luego hablar dos veces con el hada vuelves a obtener la clave.
De hecho podemos reconectar y simplemente ejecutar  
✋ 🚪/🚪🚾/🕳/🐞  
💬 🚪/🚪101/🧚  
💬 🚪/🚪101/🧚  
y obtenemos el código sin tener que hacer nada más.


## Challenge 11 - All the Possibilities
En este desafio ponen a prueba nuestros conocimientos algorítmicos de forma que, si no sabemos que algoritmo es tenemos que deducir la solución, y en caso de saberlo o averiguar cual es implementándolo debemos obtener la solución.

En este caso nos dan un número X y nos piden obtener el número total de todas las formas distintas de sumar números entre 1 y 100 para obtener dicho valor X, sin usar en el proceso ninguno de los números que pertenecen a la lista de números prohibidos.

Como he comentado la principal dificultad es saber el algoritmo a aplicar, por lo que una vez averiguas que el desafío versa sobre el algoritmo de **cambio de monedas** es fácil de resolver.  
En este caso podemos obtener una implementación en Kotlin del algoritmo (función countCoins) de http://rosettacode.org/wiki/Count_the_coins#Kotlin

```
#!kotlin
private fun solve() {
    val x = input.nextInt()
    val vetoedNumbers = input.nextLine().split(' ').filter(String::isNotBlank).map { it.toInt() }.toSet().toIntArray()
    val c = IntArray(x - 1) { it + 1 }.filter { it !in vetoedNumbers }.toIntArray()
    output.print(countCoins(c, c.size, x))
}

private fun countCoins(c: IntArray, m: Int, n: Int): Long {
    val table = LongArray(n + 1)
    table[0] = 1
    for (i in 0 until m) {
        for (j in c[i]..n) {
            table[j] += table[j - c[i]]
        }
    }
    return table[n]
}
```


## Challenge 12 - Hackerman Origins
En este desafío vuelven a poner a prueba nuestros conocimientos sobre criptografía, en este caso sobre RSA.

Para ello nos dan dos textos claros y sus correspondientes textos cifrados, y lo que nos piden es que obtengamos el módulo de la clave pública en base 10.

Por lo general, el romper el RSA suele significar que tenemos el texto cifrado y alguna implementación incorrecta y nos piden que obtengamos el texto claro.  
Dado que en este caso nos dan los textos claros y los cifrados y nos piden el módulo, es más o menos evidente que lo que tenemos que hacer es encontrar/recordar cual era la fórmula por la que a partir de textos claros y cifrados se obtenían las claves.

Como no era capaz de recordar de memoria como era la fórmula, no queda más remedio que ir a Google a buscar `finding modulus in rsa given plaintext and ciphertext`.  
*Et voila*, encontramos entre los primeros resultados un enlace a [Stackexchange](https://crypto.stackexchange.com/questions/43583/deduce-modulus-n-from-public-exponent-and-encrypted-data) donde los usuarios `user94293` y `yyyyyyy` explican como obtener el módulo a partir de los textos claros y cifrados si sabemos cual es el exponente, dando una fórmula para exponentes "pequeños" y otra para exponentes "grandes". Dado que para exponentes grandes necesitaríamos engañar al usuario para que nos cifre unos mensajes, y dado que eso no es posible suponemos que el exponente pertenece al caso "pequeño."

En nuestro caso **no** sabemos cual es el exponente usado, si bien existe una lista de exponentes habituales que son `3, 5, 17, 257, 65537`, así que para cada uno de estos exponentes aplicamos la fórmula `N = gcd(M1^e−C1,M2^e−C2)` hasta que uno devuelva el valor que queremos o todos fallen.  
Además de lo anterior, en [RosettaCode](https://rosettacode.org/wiki/RSA_code#Kotlin) tenemos un ejemplo de como convertir texto a BigInteger para poder realizar las operaciones numéricas necesarias.

```
#!kotlin
val typicalExponents = arrayOf(3, 5, 17, 257, 65537)
val plainBytes1 = File("testdata/plaintexts/test1.txt").readBytes()
val plainNum1 = BigInteger(plainBytes1)
println("plainNum1: $plainNum1")
val cipherBytes1 = File("testdata/ciphered/test1.txt").readBytes()
val cipherNum1 = BigInteger(cipherBytes1)
println("cipherNum1: $cipherNum1")
val plainBytes2 = File("testdata/plaintexts/test2.txt").readBytes()
val plainNum2 = BigInteger(plainBytes2)
println("plainNum2: $plainNum2")
val cipherBytes2 = File("testdata/ciphered/test2.txt").readBytes()
val cipherNum2 = BigInteger(cipherBytes2)
println("cipherNum2: $cipherNum2")
// N can be obtained from gcd(Me1−C1,Me2−C2).
for (e in typicalExponents) {
    println("e: $e")
    val n = (plainNum1.pow(e) - cipherNum1).gcd(plainNum2.pow(e) - cipherNum2)
    println("n: $n")
    if ((plainNum1 > n) || (plainNum2 > n)) {
        println("Plaintext is too long")
        continue
    }
    output.println(n)
    break
}
output.close()
```


## Challenge 13 - The Great Toilet Paper Fortress
Cuando llegué a este desafío era ya viernes por la noche y estaba en el puesto 85, por lo que necesitaba resolver no menos de uno, y probablemente dos si quería llegar al top 30.

El desafío consistía en dado el número de packs de papel higiénico y una serie de normas de construcción, devolver cual era la altura máxima que podíamos lograr así como cual era el máximo número de packs que podíamos usar para alcanzar dicha altura máxima.

Aunque le dediqué parte de la noche y toda la mañana del sábado no fui capaz de encontrar ni la fórmula matemática ni el algoritmo a seguir, que no fuese por fuerza bruta, por lo que a la hora de la comida abandoné este desafío y pasé al siguiente.

## Challenge 14 - Public Safety Bureau
Este desafío parecía versar sobre el protocolo [Paxos](https://en.wikipedia.org/wiki/Paxos_(computer_science)) que desconozco por completo, intenté comprender a marchas forzadas como funcionaba, para que servía y como aplicarlo al desafío.

Pasadas un par de horas sin haber conseguido avanzar nada abandoné el desafío y pasé al siguiente con el tercer y último skip disponible.


## Challenge 15 - The internsheep
Este desafío me resultó gracioso a pesar que no fui capaz de resolverlo. En él nos piden que dados unos ficheros de texto y una serie de operaciones de inserción de bytes (un byte cada vez), en cada paso devolver cual es el CRC32 del fichero.

Un detalle importante de los ficheros es que te los dan comprimidos en `TAR.GZ` y según el programa que usemos no es descomprimible.

Después de varias idas y venidas, resulta que los ficheros son especiales, de forma que tienen un tamaño lógico de varios gigas pero un tamaño físico de cero bytes.

Una vez solventado este problema obteniendo los ficheros, en mi caso particular usando Windows 10 no encontré forma de obtener por código cual es el tamaño lógico del fichero, devolviendo siempre el valor cero. Desconozco si este es un problema con Windows 10 o si simplemente es que no sé como obtener el tamaño lógico.

En cualquier caso, llegado a este punto que no fui capaz de solventar, no me quedó más remedio que esperar las 24 horas de rigor, dado que no me quedaban más skips, para poder pasar al siguiente desafío.


## Challenge 16 - The new office
Al llegar a este desafío ya era evidente que este año no llegaría al top 30.

El desafío versa sobre como distribuír grupos de personas en distintas plantas de un edificio, de forma que minimicemos el valor máximo de personas entre todas las plantas del edificio.

En principio el desafío debe de poder resolverse siguiendo la lógica de distribución de fluídos, si bien siendo ya domingo por la tarde, con el cansancio acumulado de trabajar toda la semana y de la Tuenti Challenge, y estando muy lejos de conseguir una de las camisetas, opté por dar por terminada mi andadura en la Tuenti Challenge 10, descansar un rato y luego disfrutar de la sesión de [KabutoDJ (Sebas Muriel)](https://www.twitch.tv/sebasmuriel) en Twitch.


## Challenge 17 - Zatōichi
No desbloqueado


## Challenge 18 - Matters with Formatters
No desbloqueado


## Challenge 19 - Better Farm Systems
No desbloqueado


## Challenge 20 - Fortified Security
No desbloqueado


[tc10]: https://web.archive.org/web/20200506172431/https://contest.tuenti.net/Info/about "Tuenti Challenge 10 Info"
[kotlin]: https://kotlinlang.org "Kotlin"
[stats]: https://web.archive.org/web/20200510091434/https://contest.tuenti.net/Stats "Ranking"
[git]: https://bitbucket.org/nisckis/tuenti-challenge-2020 "Repositorio Tuenti Challenge 10"
[gradle]: https://gradle.org "Gradle"

[e-c1]: https://web.archive.org/web/20200510093133/https://contest.tuenti.net/Challenges?id=1 "Desafío 1 - "
[e-c2]: https://web.archive.org/web/20200510093235/https://contest.tuenti.net/Challenges?id=2 "Desafío 2 - "
[e-c3]: https://web.archive.org/web/20200510093312/https://contest.tuenti.net/Challenges?id=3 "Desafío 3 - "
[e-c4]: https://web.archive.org/web/20200510093427/https://contest.tuenti.net/Challenges?id=4 "Desafío 4 - "
[e-c5]: https://web.archive.org/web/20200510093447/https://contest.tuenti.net/Challenges?id=5 "Desafío 5 - "
[e-c6]: https://web.archive.org/web/20200510093505/https://contest.tuenti.net/Challenges?id=6 "Desafío 6 - "
[e-c7]: https://web.archive.org/web/20200510093522/https://contest.tuenti.net/Challenges?id=7 "Desafío 7 - "
[e-c8]: https://web.archive.org/web/20200510093543/https://contest.tuenti.net/Challenges?id=8 "Desafío 8 - "
[e-c9]: https://web.archive.org/web/20200510093613/https://contest.tuenti.net/Challenges?id=9 "Desafío 9 - Just Another Day on Battlestar Galactica"
[e-c10]: https://web.archive.org/web/20200510093629/https://contest.tuenti.net/Challenges?id=10 "Desafío 10 - Villa Feristela escape room"
[e-c11]: https://web.archive.org/web/20200510093644/https://contest.tuenti.net/Challenges?id=11 "Desafío 11 - All the Possibilities"
[e-c12]: https://web.archive.org/web/20200510093704/https://contest.tuenti.net/Challenges?id=12 "Desafío 12 - Hackerman Origins"
[e-c13]: https://web.archive.org/web/20200510093721/https://contest.tuenti.net/Challenges?id=13 "Desafío 13 - The Great Toilet Paper Fortress"
[e-c14]: https://web.archive.org/web/20200510093741/https://contest.tuenti.net/Challenges?id=14 "Desafío 14 - Public Safety Bureau"
[e-c15]: https://web.archive.org/web/20200510093832/https://contest.tuenti.net/Challenges?id=15 "Desafío 15 - The internsheep"
[e-c16]: https://web.archive.org/web/20200510093849/https://contest.tuenti.net/Challenges?id=16 "Desafío 16 - The new office"
[e-c17]: https://web.archive.org/web/20200510093907/https://contest.tuenti.net/Challenges?id=17 "Desafío 17 - Zatōichi"
[e-c18]: https://web.archive.org/web/20200510093932/https://contest.tuenti.net/Challenges?id=18 "Desafío 18 - Matters with Formatters"
[e-c19]: https://web.archive.org/web/20200510093951/https://contest.tuenti.net/Challenges?id=19 "Desafío 19 - Better Farm Systems"
[e-c20]: https://web.archive.org/web/20200510094009/https://contest.tuenti.net/Challenges?id=20 "Desafío 20 - Fortified Security"
